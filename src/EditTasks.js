import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {requestAllTasks} from './redux/actions/tasks';
import Task from './Task';

import {
	Row, Col, Form, Input, Button, FormGroup, Label
} from 'reactstrap';

class EditTasks extends Component {
	componentDidMount() {
		return this.props.requestAllTasks();
	}

	render() {
		const {tasks} = this.props;

		const rows = tasks.map(
			x => <Task mode={Task.Modes.Edit} task={x} key={x._id} />
		);

		return (
			<>
				<Task mode={Task.Modes.Add} />
				{rows}
			</>
		);
	}
}

function mapStateToProps(state) {
	const {tasks} = state.tasks;
	return {tasks};
}
export default connect(
	mapStateToProps,
	dispatcher => bindActionCreators({requestAllTasks}, dispatcher)
)(EditTasks);
