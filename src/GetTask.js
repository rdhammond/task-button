import React, {Component} from 'react';
import {requestRandomTask, requestEdit} from './redux/actions/tasks';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {
	Button, Label, Row, Col
} from 'reactstrap';

class GetTask extends Component {
	constructor(props) {
		super(props);
		this.onGetTaskClick = this.onGetTaskClick.bind(this);
		this.onCompleteTaskClick = this.onCompleteTaskClick.bind(this);
	}

	onGetTaskClick() {
		this.props.requestRandomTask();
	}

	async onCompleteTaskClick() {
		await this.props.requestEdit({...this.props.currentTask, lastCompletedAt: new Date()});
		await this.props.requestRandomTask();
	}

	render() {
		const {isAllComplete, currentTask} = this.props;
		const taskText = currentTask?.text ?? (
			isAllComplete ? 'All tasks done. Good job!' : 'PRES BUTAN'
		);

		return (
			<>
				<Row>
					<Col>
						<Label size="lg" type="text text-muted">{taskText}</Label>
					</Col>
				</Row>
				<Row>
					<Col>
						<Button type="button" color="primary" size="lg"
							onClick={this.onGetTaskClick}
						>
							Get Task
						</Button>
						<Button type="button" color="success" size="lg"
							onClick={this.onCompleteTaskClick}
						>
							Complete Task
						</Button>
					</Col>
				</Row>
			</>
		);
	}
}

function mapStateToProps(state) {
	const {isAllComplete, currentTask} = state.tasks;
	return {isAllComplete, currentTask};
}
export default connect(
	mapStateToProps,
	dispatch => bindActionCreators({requestRandomTask, requestEdit}, dispatch)
)(GetTask);
