import './App.css';
import React, {Component} from 'react';
import classnames from 'classnames';
import GetTask from './GetTask';
import EditTasks from './EditTasks';

import {
	Nav, NavItem, NavLink, TabContent, TabPane
} from 'reactstrap';

const GET_TASK = 'GET_TASK';
const EDIT_TASKS = 'EDIT_TASKS';

class Main extends Component {
	constructor(props) {
		super(props);
		this.state = {activeTab: GET_TASK};
	}

	toggle(newTab) {
		this.setState({activeTab: newTab});
	}

	render() {
		const {activeTab} = this.state;

		return (
			<div>
				<Nav tabs>
					<NavItem>
						<NavLink
							className={classnames({active: activeTab === GET_TASK})}
							onClick={() => {this.toggle(GET_TASK)}}
						>
							Get Tasks
						</NavLink>
					</NavItem>
					<NavItem>
						<NavLink
							className={classnames({active: activeTab === EDIT_TASKS})}
							onClick={() => {this.toggle(EDIT_TASKS)}}
						>
							Edit Tasks
						</NavLink>
					</NavItem>
				</Nav>
				<TabContent activeTab={activeTab}>
					<TabPane tabId={GET_TASK}>
						<GetTask />
					</TabPane>
					<TabPane tabId={EDIT_TASKS}>
						<EditTasks />
					</TabPane>
				</TabContent>
			</div>
		);
	}
}

export default Main;
