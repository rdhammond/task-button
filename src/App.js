import './App.css';
import React, {Component} from 'react';
import SignIn from './SignIn';
import Main from './Main';
import AppError from './AppError';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {requestCsrf} from './redux/actions/csrf';
import {requestSelf} from './redux/actions/authorization';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {isLoaded: false};
	}

	static getDerivedStateFromError(error) {
		return {isLoaded: false};
	}

	componentDidThrow(e) {
		console.error(e.toString());
	}

	async componentDidMount() {
		await this.props.requestCsrf();

		if (this.props.hasCsrf)
			await this.props.requestSelf();

		this.setState({isLoaded: true});
	}

	render() {
		const {hasCsrf, isSignedIn} = this.props;
		const {isLoaded} = this.state;

		// ** TODO: More graceful loading screen
		if (!isLoaded)
			return <div />;

		return (
			<React.StrictMode>
				<div className="container">
					{!hasCsrf && <AppError />}
					{hasCsrf && !isSignedIn && <SignIn />}
					{hasCsrf && isSignedIn && <Main />}
				</div>
			</React.StrictMode>
		);
	}
}

function mapStateToProps(state) {
	const {token} = state.csrf;
	const {user} = state.authorization;

	return {hasCsrf: !!token, isSignedIn: !!user};
}
export default connect(
	mapStateToProps,
	dispatcher => bindActionCreators({requestCsrf, requestSelf}, dispatcher)
)(App);
