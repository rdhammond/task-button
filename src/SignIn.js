import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {signIn} from './redux/actions/authorization';

import {
	Row, Col, Form, FormGroup, Label, Input, Button, Alert
} from 'reactstrap';

class SignIn extends Component {
	constructor(props) {
		super(props);
		this.onEmailChanged = this.onEmailChanged.bind(this);
		this.onPasswordChanged = this.onPasswordChanged.bind(this);

		this.state = {email: '', password: ''};
	}

	onEmailChanged(e) {
		this.setState({email: e.currentTarget.value});
	}

	onPasswordChanged(e) {
		this.setState({password: e.currentTarget.value});
	}

	onSubmit() {
		const {email, password} = this.state;
		this.props.signIn(email, password);
	}

	render() {
		const {signInFailed} = this.props;
		const {email, password} = this.state;

		return (
			<>
				{signInFailed && 
					<Row>
						<Col>
							<Alert color="danger">
								Sign in failed. Please check your email and password.
							</Alert>
						</Col>
					</Row>
				}
				<Row>
					<Col>
						<Form>
							<FormGroup>
								<Label for="email">Email</Label>
								<Input type="email" name="email" id="email" placeholder="me@email.com" required
									value={email}
									onChange={this.onEmailChanged}
								/>
							</FormGroup>
							<FormGroup>
								<Label for="password">Password</Label>
								<Input type="password" name="password" id="password" placeholder="Password" required
									value={password}
									onChange={this.onPasswordChanged}
								/>
							</FormGroup>
							<Button disabled={!email || !password} onClick={e => this.onSubmit(e)}>Sign In</Button>
						</Form>
					</Col>
				</Row>
			</>
		);
	}
}

function mapStateToProps(state) {
	const {signInFailed} = state;
	return {signInFailed};
}
export default connect(
	mapStateToProps,
	dispatcher => bindActionCreators({signIn}, dispatcher)
)(SignIn);
