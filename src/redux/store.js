import {combineReducers, createStore, applyMiddleware} from 'redux';
import csrf from './reducers/csrf';
import authorization from './reducers/authorization';
import tasks from './reducers/tasks';
import thunk from 'redux-thunk';

const store = createStore(
	combineReducers({csrf, authorization, tasks}),
	applyMiddleware(thunk)
);
export default store;
