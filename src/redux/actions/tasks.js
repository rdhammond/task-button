import axios from '../../axios';
import {signOut} from './authorization';

export const REQUEST_RANDOM_TASK = 'REQUEST_RANDOM_TASK';
export const SET_CURRENT_TASK = 'SET_BUTTON_TASK';
export const SET_ALL_TASKS_COMPLETE = 'SET_ALL_TASKS_COMPLETE';
export const REQUEST_ALL_TASKS = 'REQUEST_ALL_TASKS';
export const SET_ALL_TASKS = 'SET_ALL_TASKS';
export const REQUEST_ADD = 'REQUEST_ADD';
export const ADD_TASK = 'ADD_TASK';
export const REQUEST_EDIT = 'REQUEST_EDIT';
export const SET_TASK_INFO = 'SET_TASK_INFO';
export const REQUEST_DELETE = 'REQUEST_DELETE';
export const REMOVE_TASK = 'REMOVE_TASK';

export function requestRandomTask() {
	return async function(dispatcher, getState) {
		await dispatcher({type: REQUEST_RANDOM_TASK});
		const task = await axios.sendWith401Check('/tasks/random');

		if (!task)
			return await dispatcher(signOut());
		if (!task._id)
			return await dispatcher(setAllTasksComplete());

		return dispatcher(setCurrentTask(task));
	};
}

export function setCurrentTask(task) {
	return { type: SET_CURRENT_TASK, task };
}

export function setAllTasksComplete() {
	return { type: SET_ALL_TASKS_COMPLETE };
}

export function requestAllTasks(token) {
	return async function(dispatcher, getState) {
		await dispatcher({type: REQUEST_ALL_TASKS});

		const query = {'$or': [
			{lastCompletedAt: {'$exists': false}},
			{nextRefresh: {'$exists': true}}
		]};

		const tasks = await axios.sendWith401Check(`/tasks?q=${JSON.stringify(query)}&sort=text`);
		await dispatcher(tasks ? setAllTasks(tasks) : signOut());
	};
}

export function setAllTasks(tasks) {
	return { type: SET_ALL_TASKS, tasks };
}

export function requestAdd(task) {
	return async function(dispatcher, getState) {
		await dispatcher({type: REQUEST_ADD});

		const newTask = await axios.sendWith401Check('/tasks', {
			data: task,
			method: 'post'
		});
		await dispatcher(newTask ? addTask(newTask) : signOut());
	};
}

export function addTask(task) {
	return { type: ADD_TASK, task };
}

export function requestEdit(task) {
	return async function(dispatcher, getState) {
		await dispatcher({type: REQUEST_EDIT});

		const newTask = await axios.sendWith401Check(`/tasks/${task._id}`, {
			data: task,
			method: 'patch'
		});
		return dispatcher(newTask ? setTaskInfo(newTask) : signOut());
	};
}

export function setTaskInfo(task) {
	return { type: SET_TASK_INFO, task };
}

export function requestDelete(task) {
	return async function(dispatcher, getState) {
		await dispatcher({type: REQUEST_DELETE});

		const oldTask = await axios.sendWith401Check(`/tasks/${task._id}`, {
			data: task,
			method: 'delete'
		});
		return dispatcher(oldTask ? removeTask(oldTask) : signOut());
	};
}

export function removeTask(task) {
	return { type: REMOVE_TASK, task };
}
