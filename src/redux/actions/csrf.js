import axios from '../../axios';

export const REQUEST_CSRF = 'REQUEST_CSRF';
export const SET_CSRF = 'SET_CSRF';
export const SET_CSRF_FAILED = 'SET_CSRF_FAILED';

export function requestCsrf() {
	return async function(dispatcher, getState) {
		await dispatcher({type: REQUEST_CSRF});

		const {data} = await axios.get('/csrf-token', {
			withCredentials: false
		});
		if (!data) return await dispatcher(setCsrfFailed());

		const {csrf} = data;
		axios.defaults.headers.post['X-CSRF-Token'] = csrf;
		axios.defaults.headers.patch['X-CSRF-Token'] = csrf;
		axios.defaults.headers.delete['X-CSRF-Token'] = csrf;
		await dispatcher(setCsrf(csrf));
	};
}

export function setCsrf(token) {
	return { type: SET_CSRF, token };
}

export function setCsrfFailed() {
	return { type: SET_CSRF_FAILED };
}
