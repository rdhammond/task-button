import axios from '../../axios';

export const REQUEST_SELF = 'REQUEST_SELF';
export const SET_SELF = 'SET_SELF';
export const SIGN_IN = 'SIGN_IN';
export const FAIL_SIGNIN = 'FAIL_SIGNIN';
export const SIGN_OUT = 'SIGN_OUT';
export const SIGNED_OUT = 'SIGNED_OUT';

export function requestSelf() {
	return async function(dispatcher, getState) {
		await dispatcher({type: REQUEST_SELF});

		const user = await axios.sendWith401Check('/users/me');
		await dispatcher(user ? setSelf(user) : signOut());
	};
}

export function setSelf(user) {
	return { type: SET_SELF, user };
};

export function signIn(username, password) {
	return async function(dispatcher, getState) {
		await dispatcher({type: SIGN_IN});

		const user = await axios.sendWith401Check('/users/signin', {
			method: 'POST',
			withCredentials: false,
			auth: {username, password}
		});
		await dispatcher(user ? setSelf(user) : failSignIn());
	};
}

export function failSignIn() {
	return { type: FAIL_SIGNIN };
}

export function signOut() {
	return async function(dispatcher, getState) {
		await dispatcher({type: SIGN_OUT});

		const result = await axios.sendWith401Check('/users/signout', {method: 'POST'});
		if (result) await dispatcher(signedOut());
	};
}

export function signedOut() {
	return { type: SIGNED_OUT };
}
