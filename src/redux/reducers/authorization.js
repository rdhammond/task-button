import * as actions from '../actions/authorization';

const initialState = {
	isBusy: false,
	user: null,
	isFailed: false,
	isErrored: false
};
const reducer = (state = initialState, action) => {
	switch (action.type) {
		case actions.REQUEST_SELF:
			return {...state,
				isBusy: true
			};
		case actions.SET_SELF:
			return {...state,
				isBusy: false,
				user: action.user,
				signInFailed: false
			};
		case actions.SIGN_IN:
			return {...state,
				isBusy: true
			};
		case actions.FAIL_SIGNIN:
			return {...state,
				isBusy: false,
				isFailed: true
			};
		case actions.SIGN_OUT:
			return {...state,
				isBusy: true
			};
		case actions.SIGNED_OUT:
			return {...state,
				isBusy: false,
				user: null
			};
		default:
			return state;
	}
};

export default reducer;
