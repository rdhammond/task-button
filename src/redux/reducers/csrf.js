import * as actions from '../actions/csrf';

const initialState = {
	token: null,
	isBusy: false,
	isErrored: false
};
const reducer = (state = initialState, action) => {
	switch (action.type) {
		case actions.REQUEST_CSRF:
			return {...state,
				isBusy: true
			};
		case actions.SET_CSRF:
			return {...state,
				token: action.token,
				isBusy: false,
				isErrored: false
			};
		case actions.SET_CSRF_FAILED:
			return {...state,
				token: null,
				isBusy: false,
				isErrored: true
			};
		default:
			return state;
	}
};

export default reducer;
