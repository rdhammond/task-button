import * as actions from '../actions/tasks';
import ReactArray from '../../utils/ReactArray';

const initialState = {
	isBusy: false,
	currentTask: null,
	tasks: [],
	isAllComplete: false
};
const reducer = (state = initialState, action) => {
	switch (action.type) {
		case actions.REQUEST_RANDOM_TASK:
			return {...state,
				isBusy: true
			};
		case actions.SET_CURRENT_TASK:
			return {...state,
				isBusy: false,
				currentTask: action.task,
				isAllComplete: false
			};
		case actions.SET_ALL_TASKS_COMPLETE:
			return {...state,
				isBusy: false,
				currentTask: null,
				isAllComplete: true
			};
		case actions.REQUEST_ALL_TASKS:
			return {...state,
				isBusy: true
			};
		case actions.SET_ALL_TASKS:
			return {...state,
				isBusy: false,
				tasks: [...action.tasks]
			};
		case actions.REQUEST_ADD:
			return {...state,
				isBusy: true
			};
		case actions.ADD_TASK:
			return {...state,
				isBusy: false,
				tasks: ReactArray.append(state.tasks, action.task)
			};
		case actions.REQUEST_EDIT:
			return {...state,
				isBusy: true
			};
		case actions.SET_TASK_INFO:
			return {...state,
				isBusy: false,
				tasks: ReactArray.replace(state.tasks, action.task)
			};
		case actions.REQUEST_DELETE:
			return {...state,
				isBusy: true
			};
		case actions.REMOVE_TASK:
			return {...state,
				isBusy: false,
				tasks: ReactArray.remove(state.tasks, action.task)
			};
		default:
			return state;
	}
};

export default reducer;
