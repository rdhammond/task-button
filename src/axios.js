import axios from 'axios';

axios.defaults.baseURL = '/api';
axios.defaults.withCredentials = true;

axios.default.sendWith401Check = async function(url, opts) {
	try {
		const resp = await axios(url, opts);
		return resp.data;
	}
	catch (e) {
		if (e.response && e.response.status === 401)
			return null;

		throw e;
	}
};

export default axios.default;
