import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {requestAdd, requestEdit, requestDelete} from './redux/actions/tasks';
import PropTypes from 'prop-types';

import {
	Row, Col, Form, Input, Button, FormGroup, Label
} from 'reactstrap';

class Task extends Component {
	static get Modes() {
		return {
			Add: 'Add',
			Edit: 'Edit'
		};
	}

	constructor(props) {
		super(props);
		this.onTaskTextChange = this.onTaskTextChange.bind(this);
		this.onAddClick = this.onAddClick.bind(this);
		this.onSaveClick = this.onSaveClick.bind(this);
		this.onDeleteClick = this.onDeleteClick.bind(this);
		this.onRepeatsChanged = this.onRepeatsChanged.bind(this);
		this.onRepeatValueChanged = this.onRepeatValueChanged.bind(this);
		this.onRepeatUnitChanged = this.onRepeatUnitChanged.bind(this);

		let state = {
			text: '',
			repeats: false,
			repeatValue: '',
			repeatUnit: '',
			nextRefresh: ''
		};

		if (props.task) {
			const {repeats, repeatValue, repeatUnit} =
				this.splitDate(props.task.repeat);

			if (props.task.nextRefresh) {
				const nextRefresh = new Date(props.task.nextRefresh);
				const date = nextRefresh.toLocaleDateString('en-US');
				const time = nextRefresh.toLocaleTimeString('en-US');
				state.nextRefresh = `${date} ${time}`;
			}

			state = {...state, repeats, repeatValue, repeatUnit};
		}

		this.state = state;
	}

	splitDate(repeat) {
		const repeats = !!repeat;
		const split = repeat?.split(' ');
		const repeatValue = split ? split[0] : '';
		const repeatUnit = split ? split[1] : '';

		return {repeats, repeatValue, repeatUnit};
	}

	onTaskTextChange(e) {
		this.setState({text: e.currentTarget.value});
	}

	getRepeat() {
		const {repeats, repeatValue, repeatUnit} = this.state;
		if (!repeats) return null;

		return `${repeatValue} ${repeatUnit}`;
	}

	onAddClick(e) {
		e.preventDefault();

		const {text} = this.state;
		const repeat = this.getRepeat();
		this.props.requestAdd({text, repeat});

		this.setState({
			text: '', 
			repeats: false,
			repeatValue: '',
			repeatUnit: ''
		});
	}

	onSaveClick(e) {
		e.preventDefault();

		const {_id} = this.props.task;
		const {text} = this.state;
		this.props.requestEdit({_id, text, repeat: this.getRepeat()});
	}

	onDeleteClick() {
		this.props.requestDelete(this.props.task);
	}

	onRepeatsChanged(e) {
		this.setState({repeats: e.currentTarget.checked});
	}

	onRepeatValueChanged(e) {
		this.setState({repeatValue: e.currentTarget.value});
	}

	onRepeatUnitChanged(e) {
		this.setState({repeatUnit: e.currentTarget.value});
	}

	render() {
		let buttons, submitHook;

		const {
			onSaveClick, onDeleteClick, onRepeatsChanged, onRepeatValueChanged,
			onRepeatUnitChanged, onAddClick
		} = this;
		const {mode, task} = this.props;
		const {text, repeats, repeatValue, repeatUnit, nextRefresh} = this.state;

		if (mode === Task.Modes.Add) {
			submitHook = onAddClick;
			buttons = (
				<Button type="submit" color="primary">Add</Button>
			);
		}
		else if (mode === Task.Modes.Edit) {
			submitHook = onSaveClick;
			buttons = (
				<>
					<Button type="submit" color="primary">Save</Button>
					<Button type="button" color="danger"
						onClick={onDeleteClick}>
						Delete
					</Button>
				</>
			);
		}
		else
			throw new Error(`Unknown/unhandled mode ${mode}.`);

		return (
			<Row>
				<Col>
					<Form inline onSubmit={submitHook}>
						{task &&
							<Input type="hidden" name="_id" value={task._id} />
						}
						<Input type="text" name="text" value={text}
							placeholder="Task Text" required maxLength="30"
							onChange={this.onTaskTextChange} />
						<FormGroup check>
							<Label check>
								<Input type="checkbox" name="repeats"
									checked={repeats} onChange={onRepeatsChanged} />{' '}
								Repeats
							</Label>
						</FormGroup>
						<Input type="number" name="repeatValue" min="1"
							value={repeatValue} onChange={onRepeatValueChanged} />
						<Input type="select" name="repeatUnit"
							value={repeatUnit} onChange={onRepeatUnitChanged}>
							<option value="days" readOnly>Days</option>
							<option value="weeks" readOnly>Weeks</option>
							<option value="months" readOnly>Months</option>
							<option value="years" readOnly>Years</option>
						</Input>
						{buttons}
						<Label>{nextRefresh}</Label>
					</Form>
				</Col>
			</Row>
		);
	}
}
Task.propTypes = {
	mode: PropTypes.oneOf(Object.keys(Task.Modes)).isRequired,

	task: function(props, propName, componentName) {
		if (props.mode === Task.Modes.Add || props.task)
			return;

		return new Error(`${propName} is required when in ${props.mode} mode.`);
	}
};

export default connect(
	null,
	dispatcher => bindActionCreators({requestAdd, requestEdit, requestDelete}, dispatcher)
)(Task);
