import React, {Component} from 'react';

export default class AppError extends Component {
	render() {
		return (
			<h1>An error occurred. Please try again later.</h1>
		);
	}
}
