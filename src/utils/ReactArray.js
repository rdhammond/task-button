function comparer(lhs, rhs, key) {
	const l = lhs[key].toLowerCase();
	const r = rhs[key].toLowerCase();

	if (l < r)
		return -1;
	if (l > r)
		return 1;
	
	return 0;
}

const ReactArray = {
	append: (arr, item, key='text') => [...arr, item].sort((l,r) => comparer(l, r, key)),
	remove: (arr, item, key='_id') => arr.filter(x => x[key] !== item[key]),
	replace: (arr, newItem, key='_id') => arr.map(oldItem => oldItem[key] === newItem[key]
		? newItem
		: oldItem
	)
};
export default ReactArray;
